package Homework7;

public class TestMyCircle {
    public static void main(String[] arge){
        MyCircle OriginCircle = new MyCircle();
        MyCircle NewCircle = new MyCircle(3, 5, 2);
        MyCircle OtherCircle = new MyCircle(NewCircle.getCenter(), 1);
        System.out.println(OtherCircle.toString());
        OtherCircle.setCenter(NewCircle.getCenter());
        int[] centerPoint = OtherCircle.getCenterXY();
        System.out.println("CircleCenterPoint=(" + centerPoint[0] + "," + centerPoint[1] + ")");
        OtherCircle.setCenterX(2);
        System.out.println("CircleNewCenterPointX=" + OtherCircle.getCenterX());
        OtherCircle.setCenterY(4);
        System.out.println("CircleNewCenterPointY=" + OtherCircle.getCenterY());
        OtherCircle.setCenterXY(5, 6);
        centerPoint = OtherCircle.getCenterXY();
        System.out.println("CircleCenterPoint=(" + centerPoint[0] + "," + centerPoint[1] + ")");       
        System.out.println("Circleradius=" + OtherCircle.getRadius());
        OtherCircle.setRadius(10);
        System.out.println("NewCircleradius=" + OtherCircle.getRadius());
        System.out.println("Area=" + OtherCircle.getArea());
        System.out.println("Circumference=" + OtherCircle.getCircumference());
        System.out.println("DistancefromOriginCircle=" + OtherCircle.distance(OriginCircle));
    }
}
