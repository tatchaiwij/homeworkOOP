package Homework7;

public class MyCircle {
    private MyPoint center;
    private int radius;

    public MyCircle(){
        this.center = new MyPoint(0,0);
        this.radius = 1;
    }
    public MyCircle(int xInput, int yInput, int radiusInput){
        this.center = new MyPoint(xInput,yInput);
        this.radius = radiusInput;
    }
    public MyCircle(MyPoint centerInput, int radiusInput){
        this.center = new MyPoint(centerInput.getX(),centerInput.getY());
        this.radius = radiusInput;
    }
    public int getRadius(){
        return this.radius;
    }
    public void setRadius(int radiusInput){
        this.radius = radiusInput;
    }
    public MyPoint getCenter(){
        return this.center;
    }
    public void setCenter(MyPoint centerPoint){
        this.center = centerPoint;
    }
    public int getCenterX(){
        return this.center.getX();
    }
    public void setCenterX(int xInput){
        this.center.setX(xInput);
    }
    public int getCenterY(){
        return this.center.getY();
    }
    public void setCenterY(int yInput){
        this.center.setY(yInput);
    }
    public int[] getCenterXY(){
        int[] out = {this.center.getX(),this.center.getY()};
        return out;
    }
    public void setCenterXY(int xInput, int yInput){
        this.center.setXY(xInput, yInput);
    }
    public String toString(){
        return "MyCircle[radius=" + this.radius + ",center=" + this.center.toString() + "]";
    }
    public double getArea(){
        return Math.PI*Math.pow(this.radius, 2);
    }
    public double getCircumference(){
        return 2*Math.PI*this.radius;
    }
    public double distance(MyCircle anothercircle){
        return this.center.distance(anothercircle.center);
    }
}
