package Homework2.my_interface;

import java.util.ArrayList;
import java.util.HashMap;

import Homework2.ToolName;

public interface ICleaner {
    public void setTools(ToolName ToolName);
    public void clean(String building, String roomName);
    public ArrayList<HashMap<String, String>> getCleanedRoom();
}
   