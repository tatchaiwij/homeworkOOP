package Homework2;

import java.util.ArrayList;
import java.util.HashMap;

import Homework2.my_interface.ICleaner;
import Homework2.my_interface.ICoachroachKiller;
import Homework2.my_interface.IWebsiteCreator;

public class AI implements IWebsiteCreator, ICleaner, ICoachroachKiller{
    public String name;
    public String language;
    public int totalkill;
    public ArrayList<HashMap<String, String>> cleanroom;
    public ToolName tool;
    public AI(String nameInput, String languageInput) {
        this.name = nameInput;
        this.language = languageInput;
        this.totalkill = 0;
        this.cleanroom = new ArrayList<>();
    }
    public void createWebsite(String template, String titleName) {
        System.out.println(language + " automated Setup template: " + template);
        System.out.println(language + " automated Set Title name: " + titleName);
    }
    public void setTools(ToolName toolName){
        this.tool = toolName;
    }
    public void clean(String building, String roomName){
        System.out.println("Start cleaning room " + roomName + " at building " + building + ".");
        HashMap<String, String> room = new HashMap<String, String>();
        room.put("building=" + building, "roomName=" + roomName);
        this.cleanroom.add(room);
    }
    public ArrayList<HashMap<String, String>> getCleanedRoom(){
        return this.cleanroom;
    }
    public void buyBaygon(int number){
        System.out.println("Buy " + number + " can of Baygon.");
    }
    public void killCoachroach(int baygonNumber, String building, String roomName){
        System.out.println("Use " + baygonNumber + " can of Baygon to kill coachroach at room " + roomName
        + " at building " + building + ".");
        this.totalkill+=100;
        this.clean(building, roomName);
    }
    public int getTotalKilled(){
        return this.totalkill;        
    }
}
   
   