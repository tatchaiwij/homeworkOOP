package Homework2;

import Homework2.my_interface.ICleaner;
import Homework2.my_interface.ICoachroachKiller;
import java.util.ArrayList;
import java.util.HashMap;

public class OfficeCleaner extends Employee implements ICleaner, ICoachroachKiller{
    public int totalkill;
    public ArrayList<HashMap<String, String>> cleanroom;
    public ToolName tool;
    public OfficeCleaner(String firstnameInput, String lastnameInput, int salaryInput) {
        super(firstnameInput, lastnameInput, salaryInput);
        this.totalkill = 0;
        this.cleanroom = new ArrayList<HashMap<String, String>>();
    }
    public void DecorateRoom(){
        System.out.println("DecorateRoom");
    }
    public void WelcomeGuest(){
        System.out.println("WelcomeGuest");
    }
    public void setTools(ToolName toolName){
        this.tool = toolName;
    }
    public void clean(String building, String roomName){
        System.out.println("Start cleaning room " + roomName + " at building " + building + ".");
        HashMap<String, String> room = new HashMap<String, String>();
        room.put("building=" + building, "roomName=" + roomName);
        this.cleanroom.add(room);
    }
    public ArrayList<HashMap<String, String>> getCleanedRoom(){
        return this.cleanroom;
    }
    public void buyBaygon(int number){
        System.out.println("Buy " + number + " can of Baygon.");
    }
    public void killCoachroach(int baygonNumber, String building, String roomName){
        System.out.println("Use " + baygonNumber + " can of Baygon to kill coachroach at room " + roomName
        + " at building " + building + ".");
        this.totalkill+=100;
        this.clean(building, roomName);
    }
    public int getTotalKilled(){
        return this.totalkill; 
    }
}
