package Homework2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import Homework2.my_interface.ICleaner;
import Homework2.my_interface.ICoachroachKiller;
import Homework2.my_interface.IWebsiteCreator;

public class Homework10_2 {
    public static void main(String[] args) {
        CEO ceo = new CEO("Captain", "Marvel", 30000);
        Programmer prog = new Programmer("Dang", "Red", 20000);
        OfficeCleaner cleaner = new OfficeCleaner ("Somsri", "Sudsuay", 10000);
        AI alphaGo = new AI("alphaGo", "Java");
        ceo.orderWebsite(prog);
        System.out.println(cleaner.getTotalKilled());
        System.out.println(alphaGo.getTotalKilled());
        ceo.orderKillCoachroach( cleaner );
        ceo.orderKillCoachroach( alphaGo );
        System.out.println(cleaner.getTotalKilled());
        System.out.println(alphaGo.getTotalKilled());
        ArrayList<HashMap<String, String>> resultByHuman = cleaner.getCleanedRoom();
        ArrayList<HashMap<String, String>> resultByAI = alphaGo.getCleanedRoom();
        System.out.println(resultByHuman.get(0));
        System.out.println(resultByAI.get(0));
        ToolName tool = ToolName.CLEANER_MACHINE;
        cleaner.setTools(tool);
        alphaGo.setTools(tool);
    }
}   