package Homework1;

import Homework1.my_interface.IWebsiteCreator;

public class Programmer extends Employee implements IWebsiteCreator{   
    public Programmer(String firstnameInput, String lastnameInput, int salaryInput) {
        super(firstnameInput, lastnameInput, salaryInput);
    }
    public void fixPC(String serialNumber) {
        System.out.println("I'm trying to fix PC serialNumber:" + serialNumber);
    }
    // สร้าง Method createWebsite()
    public void createWebsite(String template, String titleName){
        System.out.println("Start creating website name " + titleName + "using template name " + template);
    }
    // สร้าง Method installWindows()
    public void installWindows(){
        System.out.println("Install Windows");
    }
}
   
   