package Homework1;

import Homework1.my_interface.ICleaner;
import Homework1.my_interface.ICoachroachKiller;
import Homework1.my_interface.IWebsiteCreator;

public class AI implements IWebsiteCreator, ICleaner, ICoachroachKiller{
    public String name;
    public String language;
    public AI(String nameInput, String languageInput) {
        this.name = nameInput;
        this.language = languageInput;
    }
    public void createWebsite(String template, String titleName) {
        System.out.println(language + " automated Setup template: " + template);
        System.out.println(language + " automated Set Title name: " + titleName);
    }
    public void setTools(String toolName){
        System.out.println("Setup new tool name " + toolName + "."); 
    }
    public void clean(String building, String roomName){
        System.out.println("Start cleaning room " + roomName + " at building " + building + ".");
    }
    public String[] getCleanedRoom(){
        String[] arr = new String[0];
        return arr;
    }
    public void buyBaygon(int number){
        System.out.println("Buy " + number + " can of Baygon.");
    }
    public void killCoachroach(int baygonNumber, String building, String roomName){
        System.out.println("Use " + baygonNumber + " can of Baygon to kill coachroach at room " + roomName
        + " at building " + building + ".");
    }
    public int getTotalKilled(){
        return 0;        
    }
}
   
   