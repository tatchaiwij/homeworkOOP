package Homework1;

import Homework1.my_interface.ICleaner;
import Homework1.my_interface.ICoachroachKiller;

public class OfficeCleaner extends Employee implements ICleaner, ICoachroachKiller{
    public OfficeCleaner(String firstnameInput, String lastnameInput, int salaryInput) {
        super(firstnameInput, lastnameInput, salaryInput);
    }
    public void DecorateRoom(){
        System.out.println("DecorateRoom");
    }
    public void WelcomeGuest(){
        System.out.println("WelcomeGuest");
    }
    public void setTools(String toolName){
        System.out.println("Setup new tool name " + toolName + "."); 
    }
    public void clean(String building, String roomName){
        System.out.println("Start cleaning room " + roomName + " at building " + building + ".");
    }
    public String[] getCleanedRoom(){
        String[] arr = new String[0];
        return arr;
    }
    public void buyBaygon(int number){
        System.out.println("Buy " + number + " can of Baygon.");
    }
    public void killCoachroach(int baygonNumber, String building, String roomName){
        System.out.println("Use " + baygonNumber + " can of Baygon to kill coachroach at room " + roomName
        + " at building " + building + ".");
    }
    public int getTotalKilled(){
        return 0; 
    }
}
