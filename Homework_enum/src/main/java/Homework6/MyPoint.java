package Homework6;

public class MyPoint {
    private int x;
    private int y;

    public MyPoint(){
        this.x = 0;
        this.y = 0;
    }
    public MyPoint(int xInput, int yInput){
        this.x = xInput;
        this.y = yInput;
    }
    public int getX(){
        return this.x;
    }
    public void setX(int xInput){
        this.x = xInput;
    }
    public int getY(){
        return this.y;
    }
    public void setY(int yInput){
        this.y = yInput;
    }
    public int[] getXY(){
        int[] out = {this.x,this.y};
        return out;
    }
    public void setXY(int xInput, int yInput){
        this.x = xInput;
        this.y = yInput;
    }
    public String toString(){
        return "(" + this.x + "," + this.y + ")";
    }
    public double distance(int xInput, int yInput){
        int xDistance = this.x - xInput;
        int yDistance = this.y - yInput;
        return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
    }
    public double distance(MyPoint anotherpoint){
        int xDistance = this.x - anotherpoint.getX();
        int yDistance = this.y - anotherpoint.getY();
        return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
    }
    public double distance(){
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));        
    }
}
