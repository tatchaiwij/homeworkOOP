package Homework5;

public class TestBook {
    public static void main(String[] args){
        Author[] authors = {new Author("A", "A@gmail.com", 'M'),new Author("B", "B@gmail.com", 'W')};
        Book newbook = new Book("How to 101", authors, 1000000.0);
        System.out.println(newbook.toString());
        newbook = new Book("How to 101", authors, 1000000.0, 1000);
        System.out.println("Bookname=" + newbook.getName());
        System.out.println("Authersname=" + newbook.getAuthorNames());
        System.out.println("Price=" + newbook.getPrice());
        newbook.setPrice(150.0);
        System.out.println("NewPrice=" + newbook.getPrice());
        System.err.println("Qty=" + newbook.getQty());
        newbook.setQty(100);
        System.out.println("NewQty=" + newbook.getQty());
        System.out.println(newbook.toString());
    }
}
