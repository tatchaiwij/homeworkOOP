package Homework5;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qty;

    public Book(String nameInput, Author[] authorsInput, Double priceInput){
        this.name = nameInput;
        this.authors = authorsInput;
        this.price = priceInput;
    }
    public Book(String nameInput, Author[] authorsInput, Double priceInput, int qtyInput){
        this.name = nameInput;
        this.authors = authorsInput;
        this.price = priceInput;
        this.qty = qtyInput;
    }
    public String getName(){
        return this.name;
    }
    public Author[] getAuthors(){
        return this.authors;
    }
    public double getPrice(){
        return this.price;
    }
    public void setPrice(Double priceInput){
        this.price = priceInput;
    }
    public int getQty(){
        return this.qty;
    }
    public void setQty(int qtyInput){
        this.qty = qtyInput;
    }
    public String toString(){
        String out = "Book[name=n,authors={";
        for(int index = 0; index < this.authors.length; index++){
            out = out + "Author[name=" + this.authors[index].getName() + ",email=" + this.authors[index].getEmail()
            + ",gender=" + this.authors[index].getGender() + "]";
            if(index != this.authors.length - 1){
                out = out + ",";
            }
        }
        out = out + "},price=" + this.price + ",qty=" + this.qty + "]";
        return out;
    }
    public String getAuthorNames(){
        String out = "";
        for(int index = 0; index < this.authors.length; index++){
            out = out + this.authors[index].getName();
            if(index != this.authors.length - 1){
                out = out + ",";
            }
        }
        return out;
    }
}
