package Homework4;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String nameInput, String emailInput, char genderInput){
        this.name = nameInput;
        this.email = emailInput;
        this.gender = genderInput;
    }
    public String getName(){
        return this.name;
    }
    public String getEmail(){
        return this.email;
    }
    public void setEmail(String emailInput){
        this.email = emailInput;
    }
    public char getGender(){
        return this.gender;
    }
    public String toString(){
        return "Author[name=" + this.name + ",email=" + this.email + ",gender=" + this.gender + "]";
    }
}
