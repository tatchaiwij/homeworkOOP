package Homework8;

public class MyTriangle {
    private MyPoint v1;
    private MyPoint v2;
    private MyPoint v3;

    public MyTriangle(int x1Input, int y1Input, int x2Input, int y2Input, int x3Input, int y3Input){
        this.v1 = new MyPoint(x1Input,y1Input);
        this.v2 = new MyPoint(x2Input,y2Input);
        this.v3 = new MyPoint(x3Input,y3Input);
    }
    public MyTriangle(MyPoint v1Input, MyPoint v2Input, MyPoint v3Input){
        this.v1 = v1Input;
        this.v2 = v2Input;
        this.v3 = v3Input;
    }
    public String toString(){
        return "MyTriangle[v1=" + v1.toString() + ",v2=" + v2.toString() + ",v3=" + v3.toString() + "]";
    }
    public double getPerimeter(){
        return this.v1.distance(v2) + this.v1.distance(v3) + this.v2.distance(v3);
    }
    public String getType(){
        double alpha = this.v1.distance(v2);
        double beta = this.v1.distance(v3);
        double gamma = this.v2.distance(v3);
        if(alpha == beta && alpha == gamma){
            return "Equilateral";
        }else if(alpha == beta || beta == gamma || alpha == gamma){
            return "Isosceles";
        }else{
            return "Scalene";
        }
    }
}
