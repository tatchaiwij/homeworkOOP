package Homework8;

public class TestMyTriangle {
    public static void main(String[] args){
        MyTriangle newtriangle = new MyTriangle(0, 0, 1, 1, 1, 0);
        MyPoint v1Input = new MyPoint(0, 0);
        MyPoint v2Input = new MyPoint(1, 1);
        MyPoint v3Input = new MyPoint(1, 0);
        MyTriangle othertriangle = new MyTriangle(v1Input, v2Input, v3Input);
        System.out.println("PerimeterofNewTriangle=" + newtriangle.getPerimeter());
        System.out.println("TriangleTypeofOtherTriangle=" + othertriangle.getType());
    }
}
