package Homework2;

import java.util.ArrayList;

public class homework2 {
    public static void main(String[] args){
        String[] rawData = {
            "id:1001 firstname:Luck lastname:Skywalker",
            "id:1002 firstname:Tony lastname:Stark",
            "id:1003 firstname:Somchai lastname:Jaidee",
            "id:1004 firstname:MonkeyD lastname:Lufee"
        };
        ArrayList<String[]> Data = new ArrayList<>();
        for(String var : rawData){
            String[] splitstr = var.split(" ");
            String[] out = {splitstr[0], splitstr[1], splitstr[2]};
            Data.add(out);
        }
        for(String[] var : Data){
            for(String out : var){
                System.out.println(out.replaceAll(":", ": "));
            }
        }
    }    
}
