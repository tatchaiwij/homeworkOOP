package Homework4;

import java.util.ArrayList;
import java.util.HashMap;

public class homework4 {
    public static void main(String[] args){
        String[] rawData = {
            "id:1001 firstname:Luck lastname:Skywalker",
            "id:1002 firstname:Tony lastname:Stark",
            "id:1003 firstname:Somchai lastname:Jaidee",
            "id:1004 firstname:Monkey D lastname:Lufee"
        };
        ArrayList<HashMap<Integer,String>> Data = new ArrayList<>();
        ArrayList<Integer> idlist = new ArrayList<>();
        HashMap<Integer,String> map = new HashMap<Integer,String>();
        for(String var : rawData){
            String[] splitstr = var.split(":");
            map.put(Integer.parseInt(splitstr[1].replaceAll(" firstname", "")), splitstr[2].replaceAll(" lastname", "") + ":" + splitstr[3]);
            Data.add(map);
            idlist.add(Integer.parseInt(splitstr[1].replaceAll(" firstname", "")));
        }
        for(int index = 0; index < idlist.size(); index++){
            int id = idlist.get(index);
            System.out.println("id: " + id);
            System.out.println("firstname: " + map.get(id).split(":")[0]);
            System.out.println("lastname:: " + map.get(id).split(":")[1]);
        }
    }
}
