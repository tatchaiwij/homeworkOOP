package Homework1;

import java.util.ArrayList;
import java.util.HashMap;

public class homework1 {
    public static void main(String[] args){
        String[] rawData = {
            "id:1001 firstname:Luck lastname:Skywalker",
            "id:1002 firstname:Tony lastname:Stark",
            "id:1003 firstname:Somchai lastname:Jaidee",
            "id:1004 firstname:MonkeyD lastname:Lufee"
        };
        //1.1
        ArrayList<HashMap<Integer,String>> Data = new ArrayList<>();
        ArrayList<Integer> idlist = new ArrayList<>();
        HashMap<Integer,String> map = new HashMap<Integer,String>();
        for(String var : rawData){
            String[] splitstr = var.split(" ");
            map.put(Integer.parseInt(splitstr[0].split(":")[1]), splitstr[1] + " " + splitstr[2]);
            Data.add(map);
            idlist.add(Integer.parseInt(splitstr[0].split(":")[1]));
        }
        //1.2
        for(int index = 0; index < idlist.size(); index++){
            int id = idlist.get(index);
            System.out.println("id: " + id);
            System.out.println(map.get(id).split(" ")[0].replaceAll(":", ": "));
            System.out.println(map.get(id).split(" ")[1].replaceAll(":", ": "));
        }
    }
}
