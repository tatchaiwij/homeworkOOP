package Homework3;

import java.util.ArrayList;

public class CountAnimal {
    public static void main(String[] args){
        ArrayList<Animal> zooAnimal = new ArrayList<>();
        int tigerAmount = 3;
        int penguinAmount = 5;
        int hippoAmount = 4;
        for(int i = 0; i < tigerAmount; i++){
            zooAnimal.add(new Animal("Tiger"));
        }
        for(int i = 0; i < penguinAmount; i++){
            zooAnimal.add(new Animal("Penguin"));
        }
        for(int i = 0; i < hippoAmount; i++){
            zooAnimal.add(new Animal("Hippo"));
        }
        int tiger = 0;
        int penguin = 0;
        int hippo = 0;
        for(Animal animal : zooAnimal){
            if(animal.getName().equals("Tiger")){
                tiger++;
            }else if(animal.getName().equals("Penguin")){
                penguin++;
            }else if(animal.getName().equals("Hippo")){
                hippo++;
            }
        }
        System.out.println("Tiger: " + tiger);
        System.out.println("Penguin: " + penguin);
        System.out.println("Hippo: " + hippo);
    }
}
