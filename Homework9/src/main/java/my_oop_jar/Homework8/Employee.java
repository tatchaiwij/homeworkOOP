package my_oop_jar.Homework8;

public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private int salary;

    public Employee(int idInput, String firstnameInput, String lastnameInput, int salaryInput){
        this.id = idInput;
        this.firstName = firstnameInput;
        this.lastName = lastnameInput;
        this.salary = salaryInput;
    }
    public int getID(){
        return this.id;
    }
    public String getFirstname(){
        return this.firstName;
    }
    public String getName(){
        return this.firstName + " " + this.lastName;
    }
    public int getSalary(){
        return this.salary;
    }
    public void setSalary(int salaryInput){
        this.salary = salaryInput;
    }
    public int getAnnualSalary(){
        return this.salary*12;
    }
    public int raiseSalary(int percent){
        int newsalary = (int)(percent*(this.salary/100.0) + this.salary);
        this.salary = newsalary;
        return this.salary;
    }
    public String toString(){
        return "Employee[id=" + this.id + ",name=" + this.firstName + " " + this.lastName + ",salary=" + this.salary + "]";
    }
}

