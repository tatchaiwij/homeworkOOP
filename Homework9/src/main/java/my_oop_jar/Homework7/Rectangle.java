package my_oop_jar.Homework7;

public class Rectangle {
    private float length;
    private float width;

    public Rectangle(){
        this.length = 1.0f;
        this.width = 1.0f;
    }
    public Rectangle(float lengthInput, float widthInput){
        this.length = lengthInput;
        this.width = widthInput;
    }
    public float getLength(){
        return this.length;
    }
    public void setLength(float lengthInput){
        this.length = lengthInput;
    }
    public float getWidth(){
        return this.width;
    }
    public void setWidth(float widthInput){
        this.width = widthInput;
    }
    public double getArea(){
        return width*length;
    }
    public double getPerimeter(){
        return (2*width)+(2*length);
    }
    public String getString(){
        return "Rectangle[length=" + this.length + ",width=" + this.width + "]";
    }
}
