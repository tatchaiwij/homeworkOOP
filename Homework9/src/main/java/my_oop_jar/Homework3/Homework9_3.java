package my_oop_jar.Homework3;

public class Homework9_3 {
    public static void main(String[] args) {
        String[] rawData = {
            "id:1001 firstname:Luke lastname:Skywalker salary:10000 type:frontend",
            "id:1002 firstname:Tony lastname:Stark salary:20000 type:backend",
            "id:1003 firstname:Somchai lastname:Jaidee salary:30000 type:fullstack",
            "id:1004 firstname:MonkeyD lastname:Luffee salary:40000 type:fullstack"
        };
        CEO marvel = new CEO("Captain", "Marvel", 30000);
        ProgrammerAdd(marvel, rawData);
        for(int index = 0; index < marvel.employees.length; index++){
            System.out.println("ID:" + marvel.employees[index].id + " Firstname:" + marvel.employees[index].firstname + " Lastname:" + marvel.employees[index].lastname
            + " Salary:" + marvel.employees[index].getSalary() + " Type:" + marvel.employees[index].type);
        }
    }
    public static void ProgrammerAdd (CEO ceo, String[] rawData){
        int oldarraylength = ceo.employees.length;
        Programmer[] newprogrammerarr = new Programmer[oldarraylength + rawData.length];
        for (int index = 0; index < oldarraylength; index++){
            newprogrammerarr[index] = ceo.employees[index];
        }
        for (int index = 0; index < rawData.length; index++){
            String[] spilt = rawData[index].split(" ");
            Programmer newprogrammer = new Programmer(Integer.parseInt(spilt[0].split(":")[1]),spilt[1].split(":")[1],spilt[2].split(":")[1],
            Integer.parseInt(spilt[3].split(":")[1]),spilt[4].split(":")[1]);
            newprogrammerarr[oldarraylength + index] = newprogrammer;
        }
        ceo.employees = newprogrammerarr;
    }
}
