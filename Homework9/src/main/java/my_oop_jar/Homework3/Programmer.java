package my_oop_jar.Homework3;

public class Programmer extends Employee {
    public int id;
    public String type;

    public Programmer(int idInput, String firstnameInput, String lastnameInput, int salaryInput, String typeInput) {
        super(firstnameInput, lastnameInput, salaryInput);
        this.id = idInput;
        this.type = typeInput;
    }
}
