package my_oop_jar.Homework5;

public class Circle {
    private double radius;
    private String color;

    public Circle(){
        this.radius = 1.0;
        this.color = "red";
    }
    public Circle(double radiusInput){
        this.radius = radiusInput;
    }
    public double getRadius(){
        return this.radius;
    }
    public double getArea(){
        return Math.PI*Math.pow(this.radius, 2);
    }
}
