package my_oop_jar.Homework4;

public class CEO extends Employee {
    public String dressCode;
    public CEO(String firstnameInput, String lastnameInput, int salaryInput, int idInput, String dressCodeInput) {
        super(firstnameInput, lastnameInput, salaryInput, idInput);
        this.dressCode = dressCodeInput;
    }
    public Employee [] employees = new Employee[0];
    public int getSalary(){
        return super.getSalary()*2;
    };
    public void work (Employee luckyEmployee) {
        this.fire(luckyEmployee);
        this.hire(luckyEmployee);
        this.seminar();
        this.golf();
    }
    public void assignNewSalary(Employee luckyEmployee, int newSalary) {
        // เขียนให้ตรวจว่าเงินเดือนน้อยกว่าเดิมหรือไม่ 
        // หากเงินเดือนน้อยกว่าเดิมให้ขึ้นข้อความว่า <ชื่อพนักงาน>'s salary is less than before!! และไม่ต้อง set เงินเดือนใหม่ให้พนักงานคนนั้น
        // หากเงินเดือนมากกว่าเดิมให้ set เงินเดือนใหม่ให้สำเร็จ และขึ้นข้อความว่า <ชื่อพนักงาน>'s salary has been set to <newSalary>
        if(luckyEmployee.getSalary() < newSalary){
            System.out.println(luckyEmployee.firstname + "'s salary has been set to " + newSalary);
            luckyEmployee.setSalary(newSalary);
        }else{
            System.out.println(luckyEmployee.firstname + "'s salary is less than before!!");
        }
    }
    private void golf () { // simulate private method
        this.dressCode = "golf_dress";
        System.out.println("He goes to golf club to find a new connection. Dress with :" + this.dressCode);
    };
    public void fire(Employee luckyEmployee){
        System.out.println(luckyEmployee.firstname + " has been fired! Dress with :" + this.dressCode);
    }
    public void hire(Employee luckyEmployee){
        System.out.println(luckyEmployee.firstname + " has been hired back! Dress with :" + this.dressCode);
    }
    public void seminar(){
        this.dressCode = "suit";
        System.out.println("He is going to seminar Dress with :" + this.dressCode);
    }
}
