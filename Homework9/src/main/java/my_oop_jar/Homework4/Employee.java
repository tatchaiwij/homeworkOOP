package my_oop_jar.Homework4;

public class Employee {
    public   int id;
    public   String firstname;
    public   String lastname;
    private  int salary;
    public   String type;
    public   String dressCode;
   
    public Employee(String firstnameInput, String lastnameInput, int salaryInput, int idInput) {
        this.firstname = firstnameInput;
        this.lastname = lastnameInput;
        this.salary = salaryInput;
        this.id = idInput;
    }
    public void hello() {
        System.out.println("Hello " + this.firstname );
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public void gossip(Employee employee, String out){
        System.out.println("Hey " + employee.firstname + ", " + out);
    }
    public void work(){}
}
