package my_oop_jar.Homework4;

public class OfficeCleaner extends Employee {
    public String dressCode;
    public OfficeCleaner(String firstnameInput, String lastnameInput, int salaryInput, int idInput, String dressCodeInput) {
        super(firstnameInput, lastnameInput, salaryInput, idInput);
        this.dressCode = dressCodeInput;
    }
    public void work(){
        Clean();
        KillCoachroach();
        DecorateRoom();
        WelcomeGuest();
    }
    public void Clean(){
        System.out.println(this.firstname + " is cleaning a room.");
        System.out.println("\"The objective of cleaning is not just to clean, but to feel happiness living within that environment.\" Marie Kondo");
        System.out.println("");
    }
    public void KillCoachroach(){
        System.out.println(this.firstname + " is trying to kill coachroach.");
        System.out.println("Hello there, Peter.");
        System.out.println("");
    }
    public void DecorateRoom(){
        System.out.println(this.firstname + " is decorate a new room.");
        System.out.println("\"Everything has a place, and everything in its place.\"");
        System.out.println("");
    }
    public void WelcomeGuest(){
        System.out.println(this.firstname + " is welcoming our guest");
        System.out.println("Welcome To The Danger Zone.");
    }
}
