package my_oop_jar.Homework4;

public class Programmer extends Employee {
    public String type;
    public Programmer(String firstnameInput, String lastnameInput, int salaryInput, int idInput, String typeInput) {
        super(firstnameInput, lastnameInput, salaryInput, idInput);
        this.type = typeInput;
    }
    public void CreateWebsite(){
        System.out.println(this.firstname + " is now creating new website");
        System.out.println("Another E-commerce website INCOMING!!");
        System.out.println();
    }
    public void FixPC(){
        System.out.println(this.firstname + " is fixing another PC");
        System.out.println(":( Your PC ran into a problem and needs to restart as soon as we're finished collecting some error info.");
        System.out.println("Here's a useless code that Google has no results for. :)");
        System.out.println();
    }
    public void InstallWindows(){
        System.out.println(this.firstname + " is now installing a new copy of Windows on new PC");
        System.out.println("Hi there! I'm Cortana, and I'm here to help.");
        System.out.println("A little sign-in here, a touch of Wi-Fi there, and we'll have your PC ready for all you plan to do.");
        System.out.println();
    }
    public void work(){
        CreateWebsite();
        FixPC();
        InstallWindows();
    }
}
