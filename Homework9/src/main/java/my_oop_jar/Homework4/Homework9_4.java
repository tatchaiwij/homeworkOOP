package my_oop_jar.Homework4;

public class Homework9_4 {
    public static void main(String[] args) {
        String[] rawData = {
            "id:1001 firstname:Luke lastname:Skywalker salary:10000 type:frontend role:Programmer",
            "id:1002 firstname:Tony lastname:Stark salary:20000 type:tshirt role:CEO",
            "id:1003 firstname:Somchai lastname:Jaidee salary:30000 type:fullstack role:Programmer",
            "id:1004 firstname:MonkeyD lastname:Luffee salary:40000 type:maid role:OfficeCleaner"
        };
        CEO newceo = new CEO("firstname", "lastnameInput", '0', '0', "dressCode");
        for(int index = 0; index < rawData.length; index++){
            String[] spilt = rawData[index].split(" ");
            if(spilt[5].split(":")[1].equals("CEO")){
                newceo = new CEO(spilt[1].split(":")[1], spilt[2].split(":")[1], Integer.parseInt(spilt[3].split(":")[1]),
                Integer.parseInt(spilt[0].split(":")[1]), spilt[4].split(":")[1]);
                assignrole(rawData, newceo);
                break;
            }
        }
        for(int index = 0; index < newceo.employees.length; index++){
            newceo.employees[index].work();
        }
    }
    public static void assignrole(String[] rawData, CEO newceo){
        for(int index = 0; index < rawData.length; index++){
            String[] spilt = rawData[index].split(" ");
            String role = spilt[5].split(":")[1];
            if(role.equals("Programmer") || role.equals("OfficeCleaner"))
            {
                addemployees(newceo, spilt, role);
            }
        }
    }
    public static void addemployees(CEO ceo, String[] spilt, String role){
        int oldarraylength = ceo.employees.length;
        Employee[] newemployees = new Employee[oldarraylength + 1];
        for (int index = 0; index < oldarraylength; index++){
            newemployees[index] = ceo.employees[index];
        }
        if(role.equals("Programmer")){
            Programmer newProgrammers = new Programmer(spilt[1].split(":")[1], spilt[2].split(":")[1], Integer.parseInt(spilt[3].split(":")[1]),
            Integer.parseInt(spilt[0].split(":")[1]), spilt[4].split(":")[1]);
            newemployees[oldarraylength] = newProgrammers;
        }else if(role.equals("OfficeCleaner")){
            OfficeCleaner newOfficeCleaner = new OfficeCleaner(spilt[1].split(":")[1], spilt[2].split(":")[1], Integer.parseInt(spilt[3].split(":")[1]),
            Integer.parseInt(spilt[0].split(":")[1]), spilt[4].split(":")[1]);
            newemployees[oldarraylength] = newOfficeCleaner;
        }
        ceo.employees = newemployees;
    }
}
