package Homework3;

import Homework3.Interface.ICustomer;
import Homework3.Interface.IVisit;
import java.util.Date;

public class Visit implements IVisit{
    private ICustomer customer;
    private Date date;
    private double serviceExpense;
    private double productExpense;

    public Visit(ICustomer customerInput, Date dateInput){
        customer = customerInput;
        date = dateInput;
    }
    public String getName(){
        return this.customer.getName();
    }
    public double getServiceExpense(){
        return this.serviceExpense - (this.serviceExpense*DiscountRate.getServiceDiscountRate(this.customer.getMemberType()));
    }
    public void setServiceExpense(double serviceExpenseInput){
        this.serviceExpense = serviceExpenseInput;
    }
    public double getProductExpense(){
        return this.productExpense - (this.productExpense*DiscountRate.getProductDiscountRate(this.customer.getMemberType()));
    }
    public void setProduceExpense(double productExpenseInput){
        this.productExpense = productExpenseInput;
    }
    public double getTotalExpense(){
        return this.serviceExpense + this.productExpense - (this.productExpense*DiscountRate.getProductDiscountRate(this.customer.getMemberType()))
        - (this.serviceExpense*DiscountRate.getServiceDiscountRate(this.customer.getMemberType()));
    }
    public String toString(){
        return "CustomerVisit=[Date=" + this.date + ",CustomerName=" + this.customer.getName()
        + ",ServiceExpense=" + this.getServiceExpense() + "Bath,ProductExpense=" + this.getProductExpense() + "Bath,TotalExpense=" + this.getTotalExpense() + "Bath]";
    }
}
