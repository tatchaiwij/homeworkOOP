package Homework3.Interface;

public interface IVisit {
    public double getServiceExpense();
    public double getProductExpense();
    public double getTotalExpense();
    public void setServiceExpense(double serviceExpenseInput);
    public void setProduceExpense(double productExpenseInput);
    public String getName();
    public String toString();
}
