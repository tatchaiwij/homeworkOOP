package Homework3.Interface;

public interface ICustomer {
    public String getName();
    public String getMemberType();
    public boolean isMember();
    public void setMember(boolean memberInput);
    public void setMemberType(String membertypeInput);
    public String toString();
}
