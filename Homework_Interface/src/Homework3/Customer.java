package Homework3;

import Homework3.Interface.ICustomer;

public class Customer implements ICustomer{
    private String name;
    private boolean member;
    private String memberType;

    public Customer(String nameInput){
        this.name = nameInput;
        this.member = false;
        this.memberType = "None";
    }
    public String getName(){
        return this.name;
    }
    public boolean isMember(){
        return this.member;
    }
    public void setMember(boolean memberInput){
        this.member = memberInput;
    }
    public String getMemberType(){
        return this.memberType;
    }
    public void setMemberType(String membertypeInput){
        this.memberType = membertypeInput;
    }
    public String toString(){
        return "Customer[Name=" + this.name + ",Membertype=" + this.memberType + "]";
    }
}
