package Homework3;

import Homework3.Interface.ICustomer;
import Homework3.Interface.IVisit;
import java.util.Date;

public class Homework3 {
    public static void main(String[] args){
        int $x = 5;
        int $xPrice = 100;
        int $y = 2; 
        int $yPrice = 2500;
        ICustomer newcustomer = new Customer("Leo");
        Date date = new Date();
        IVisit newvisit = new Visit (newcustomer, date);
        System.out.println("NewCustomer[Name=" + newcustomer.getName() + ",IsMember=" + newcustomer.isMember() + "]");
        System.out.println("VisitLog[Date=" + date + ",CustomerName=" + newvisit.getName() + "]");
        System.out.println("Customer buy " + $x + " products and " + $y + " services.");
        newvisit.setProduceExpense($x*$xPrice);
        newvisit.setServiceExpense($y*$yPrice);
        System.out.println(newvisit.toString());
        System.out.println("Customer designed to signup for membership.");
        newcustomer.setMember(true);
        newcustomer.setMemberType("Silver");
        System.out.println("Now customer " + newcustomer.getName() + " membership level is " + newcustomer.getMemberType() + ".");
        System.out.println(newcustomer.toString());
        System.out.println(newvisit.toString());
        System.out.println("Final expense for customer " + newvisit.getName() + " is " + newvisit.getTotalExpense() + " Bath.");
    }
}
