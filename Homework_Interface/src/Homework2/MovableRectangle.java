package Homework2;
import Homework2.Interface.Movable;

public class MovableRectangle implements Movable{
    private MovablePoint topLeft;
    private MovablePoint bottomRight;

    public MovableRectangle(int x1Input, int y1Input, int x2Input, int y2Input, int xSpeedInput, int ySpeedInput){
        this.topLeft = new MovablePoint(x1Input, y1Input, xSpeedInput, ySpeedInput);
        this.bottomRight = new MovablePoint(x2Input, y2Input, xSpeedInput, ySpeedInput);
    }
    public void moveUp(){
        this.topLeft.moveUp();
        this.bottomRight.moveUp();
    }
    public void moveDown(){
        this.topLeft.moveDown();
        this.bottomRight.moveDown();
    }
    public void moveRight(){
        this.topLeft.moveRight();
        this.bottomRight.moveRight();
    }
    public void moveLeft(){
        this.topLeft.moveLeft();
        this.bottomRight.moveLeft();
    }
    public String toString(){
        return "MovableRectangle";
    }
}
