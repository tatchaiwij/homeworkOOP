package Homework2;
import Homework2.Interface.Movable;

public class MovablePoint implements Movable{
    private int x;
    private int y;
    private int xSpeed;
    private int ySpeed;

    public MovablePoint(int xInput, int yInput, int xSpeedInput, int ySpeedInput){
        this.x = xInput;
        this.y = yInput;
        this.xSpeed = xSpeedInput;
        this.ySpeed = ySpeedInput;
    }
    public String toString(){
        return "MovablePoint[X=" + this.x + ",Y=" + this.y + ",xSpeed=" + this.xSpeed + ",ySpeed=" + this.ySpeed + "]";
    }
    public void moveUp(){
        this.y = this.y + this.ySpeed;
    }
    public void moveDown(){
        this.y = this.y - this.ySpeed;
    }
    public void moveLeft(){
        this.x = this.x - this.xSpeed;
    }
    public void moveRight(){
        this.x = this.x + this.xSpeed;
    }
}
