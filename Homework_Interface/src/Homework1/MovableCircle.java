package Homework1;
import Homework1.Interface.Movable;

public class MovableCircle implements Movable{
    private int radius;
    private MovablePoint center;

    public MovableCircle(int xInput, int yInput, int xSpeedInput, int ySpeedInput, int radiusInput){
        this.center = new MovablePoint(xInput, yInput, xSpeedInput, ySpeedInput);
        this.radius = radiusInput;
    }
    public void moveUp(){
        this.center.moveUp();
    }
    public void moveDown(){
        this.center.moveDown();
    }
    public void moveRight(){
        this.center.moveRight();
    }
    public void moveLeft(){
        this.center.moveLeft();
    }
    public String toString(){
        return "MovableCircle[Radius=" + this.radius + "]";
    }
}
