package e_commerce;

import java.util.ArrayList;

import e_commerce.Abstract.aManage;
import e_commerce.Interface.ifile;
import e_commerce.Interface.iproduct;
import e_commerce.Interface.iproductList;

public class manageMode extends aManage {
    private iproductList productlist;
    private ArrayList<iproduct> listofproduct;
    private ifile file;
    public manageMode(){
        this.file = new file();
        this.productlist = new ProductList();
        this.listofproduct = this.productlist.listProduct();
    }
    public void manageProduct(){
        if(this.listofproduct.size() > 0){
            for(iproduct var : this.listofproduct){
                System.out.println("Product[ID=" + var.getID() + ",Name=" + var.getName() + ",Price=" + var.getPrice() + "]");
            }
        }
        printChoice();
        String input = App.readInput();
        switch (input){
            case "A":
                System.out.println("Please insert product Name.");
                String terminal_input_Str = App.readInput();
                Boolean check = false;
                do{
                    System.out.println("Please insert product Price.");
                    try{  
                        double terminal_input_Int = Double.parseDouble(App.readInput());
                        file.writeList(terminal_input_Str, terminal_input_Int);
                        this.productlist = new ProductList();
                        this.listofproduct = this.productlist.listProduct();
                        check = true;
                    }catch(NumberFormatException e){ 
                        System.out.println("Invalid input.");
                    } 
                }while(check == false);
                break;
            case "B":;
                if(listofproduct.size() > 0){
                    Boolean checkID = false;
                    do{
                        System.out.println("Please insert product ID.");
                        try{  
                            int terminal_input_Int = Integer.parseInt(App.readInput());
                            file.removeList(terminal_input_Int);
                            checkID = true;
                        }catch(NumberFormatException e){ 
                            System.out.println("Invalid input.");
                            for(iproduct var : this.listofproduct){
                                System.out.println("Product[ID=" + var.getID() + ",Name=" + var.getName() + ",Price=" + var.getPrice() + "]");
                            }
                        } 
                    }while(checkID == false);
                    this.productlist = new ProductList();
                    this.listofproduct = this.productlist.listProduct();
                }else{
                    System.out.println("There is no product in the system.");
                }
                break;
            case "BACK":
                App.usermode = Mode.NULL;
                break;
            default:
                System.out.println("Invalid input.");
                break;
        }
    }
}
