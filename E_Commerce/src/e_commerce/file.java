package e_commerce;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import e_commerce.Interface.ifile;

public class file implements ifile{
    private final String currentDirectory = System.getProperty("user.dir");
    private final Path directoryPath = Paths.get(currentDirectory, "res");
    private final Path productlistfilePath = Paths.get(directoryPath.toString(), "productlist.csv");
    private final Path invoicefilePath = Paths.get(currentDirectory.toString(), "invoice.txt");
    private final File directory = new File(this.directoryPath.toString());
    private final File filedirectory = new File(this.productlistfilePath.toString());
    private final File invoicedirectory = new File(this.invoicefilePath.toString());
    public boolean checkDirectoryExist(){
        if(Files.exists(this.directoryPath)){
            return true;
        }else{
            return false;
        }
    }
    public void createDirectory(){
        this.directory.mkdirs();
    }
    public boolean checkFileExits(){
        if(filedirectory.isFile()){
            return true;
        }else{
            return false;
        }
    }
    public void writeList(String productNameInput, double terminal_input_Int){
        ArrayList<String> productList = new ArrayList<String>();
        try {
            Scanner Reader = new Scanner(this.filedirectory);
            while (Reader.hasNextLine()) {
                String data = Reader.nextLine();
                productList.add(data);
            }
            Reader.close();
        }catch(FileNotFoundException e){}
        productList.add(productNameInput + "," + terminal_input_Int);
        try{
            FileWriter writer = new FileWriter(filedirectory);
            for (int index = 0; index < productList.size(); index++) {
                writer.append(productList.get(index));
                writer.append("\n");
            }
            writer.close();
        }catch(IOException e){}
    }
    public void removeList(int product){
        ArrayList<String> productList = new ArrayList<String>();
        try {
            Scanner Reader = new Scanner(this.filedirectory);
            while (Reader.hasNextLine()) {
                String data = Reader.nextLine();
                productList.add(data);
            }
            Reader.close();
        }catch(FileNotFoundException e){}
        productList.remove(product - 1);
        try{
            FileWriter writer = new FileWriter(filedirectory);
            for (int index = 0; index < productList.size(); index++) {
                writer.append(productList.get(index));
                writer.append("\n");
            }
            writer.close();
        }catch(IOException e){}
    }
    public ArrayList<String> readList(){
        ArrayList<String> productList = new ArrayList<String>();
        try {
            Scanner Reader = new Scanner(this.filedirectory);
            while (Reader.hasNextLine()) {
                String data = Reader.nextLine();
                productList.add(data);
            }
            Reader.close();
        }catch(FileNotFoundException e){}
        return productList;
    }
    public void writeAddress(ArrayList<String> Input){
        try{
            FileWriter writer = new FileWriter(invoicedirectory);
            for (int index = 0; index < Input.size(); index++) {
                writer.append(Input.get(index));
                writer.append("\n");
            }
            writer.close();
        }catch(IOException e){}
    }
}
