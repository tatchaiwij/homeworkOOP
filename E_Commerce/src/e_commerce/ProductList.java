package e_commerce;

import java.util.ArrayList;

import e_commerce.Interface.ifile;
import e_commerce.Interface.iproduct;
import e_commerce.Interface.iproductList;

public class ProductList implements iproductList {
    ifile file;
    private ArrayList<iproduct> productlist;
    public ProductList(){
        this.productlist = new ArrayList<iproduct>();
        this.file = new file();
        if(!this.file.checkDirectoryExist()){
            this.file.createDirectory();
        }else{
            if(this.file.checkFileExits()){
                ArrayList<String> productList = this.file.readList();
                for(int index = 0; index < productList.size(); index++){
                    Product newProduct = new Product(productList.get(index).split(",")[0], index + 1, Double.parseDouble(productList.get(index).split(",")[1])); 
                    this.productlist.add(newProduct);
                }
            }
        }
    }
    public ArrayList<iproduct> listProduct(){
        return this.productlist;
    }
}
