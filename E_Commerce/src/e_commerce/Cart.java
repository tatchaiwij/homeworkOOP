package e_commerce;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import e_commerce.Interface.icart;
import e_commerce.Interface.ifile;
import e_commerce.Interface.iproduct;

public class Cart implements icart{
    private ArrayList<iproduct> productlist;
    public Cart(){
        this.productlist = new ArrayList<iproduct>();
    }
    public void addTo(iproduct productInput){
        this.productlist.add(productInput);
    }
    public void removeFrom(iproduct productInput){
        if(productlist.size() == 1){
            this.productlist = new ArrayList<iproduct>();
        }else{
            this.productlist.remove(productInput);
        }
    }
    public ArrayList<iproduct> cartList(){
        return this.productlist;
    }
    public void Checkout(ArrayList<String> addressInput){
        ifile file = new file();
        ArrayList<String> out = new ArrayList<String>();
        Set<iproduct> uniqueProduct = new HashSet<iproduct>(this.productlist);
        Date date = new Date();
        double totalprice = 0;
        out.add("Invoice");
        out.add("Date: " + date);
        out.add("Address:");
        out.addAll(addressInput);
        out.add("Product List:");
        for(iproduct var : uniqueProduct){
            int count = Collections.frequency(this.productlist, var);
            totalprice+=var.getPrice()*count;
            out.add("ID: " + var.getID() + " Name: " + var.getName() + " Qty: " + count + " Price Per Unit: " + var.getPrice()
            + " Baht TotalPrice: " + var.getPrice()*count + " Baht");
        }
        out.add("Total Price = " + totalprice + " Bath");
        file.writeAddress(out);
    }
}
