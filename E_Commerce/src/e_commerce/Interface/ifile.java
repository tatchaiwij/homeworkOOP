package e_commerce.Interface;

import java.util.ArrayList;

public interface ifile {
    public boolean checkDirectoryExist();
    public boolean checkFileExits();
    public void createDirectory();
    public void writeList(String productName, double terminal_input_Int);
    public void removeList(int product);
    public ArrayList<String> readList();
    public void writeAddress(ArrayList<String> AddressInput);
}
