package e_commerce.Interface;

public interface iproduct {
    public String getName();
    public int getID();
    public iproduct getProduct();
    public double getPrice();
}
