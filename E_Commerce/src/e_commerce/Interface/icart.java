package e_commerce.Interface;

import java.util.ArrayList;

public interface icart {
    public void addTo(iproduct product);
    public void removeFrom(iproduct product);
    public ArrayList<iproduct> cartList();
    public void Checkout(ArrayList<String> addressInput);
}
