package e_commerce;

import e_commerce.Interface.iproduct;

public class Product implements iproduct{
    private String productName;
    private int productID;
    private double price;
    public Product(String nameInput, int idInput, double priceInput){
        this.productName = nameInput;
        this.productID = idInput;
        this.price = priceInput;
    }
    public String getName(){
        return this.productName;
    }
    public int getID(){
        return this.productID;
    }
    public iproduct getProduct(){
        return this;
    }
    public double getPrice(){
        return this.price;
    }
}
