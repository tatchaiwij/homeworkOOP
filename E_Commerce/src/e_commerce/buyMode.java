package e_commerce;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import e_commerce.Abstract.aBuy;
import e_commerce.Interface.icart;
import e_commerce.Interface.iproduct;
import e_commerce.Interface.iproductList;

public class buyMode extends aBuy {
    private icart cartInput;
    private ArrayList<iproduct> list;
    private iproductList productlist;
    private ArrayList<iproduct> listofproduct;
    private Set<iproduct> uniqueProduct;
    public buyMode(){
        this.cartInput = new Cart();
        this.productlist = new ProductList();
        this.listofproduct = this.productlist.listProduct();
    }
    public void buyProduct(String input){
        switch (input){
            case "A":
                if(this.listofproduct.size() > 0){
                    boolean check = false;
                    do{
                        for(iproduct var : this.listofproduct){
                            System.out.println("ID: " + var.getID() + " Name: " + var.getName() + " Price Per Unit: " + var.getPrice()+ " Baht");
                        }
                        System.out.println("Please insert product ID.");
                        try{  
                            int terminal_input = Integer.parseInt(App.readInput());
                            for(iproduct var : this.listofproduct){
                                if(var.getID() == terminal_input){
                                    boolean checkqty = false;
                                    do{
                                        System.out.println("Please insert Qty.");
                                        try{
                                            int terminal_input_qty = Integer.parseInt(App.readInput());
                                            System.out.println("Add " + terminal_input_qty + " " + var.getName() + " into the cart.");
                                            for(int index = 0; index < terminal_input_qty; index++){
                                                    this.cartInput.addTo(var);
                                            }
                                            checkqty = true;
                                            break;
                                        }catch(NumberFormatException e){ 
                                            System.out.println("Invalid Qty.");
                                        }
                                    }while(checkqty == false);
                                }
                            }
                            check = true;
                        }catch(NumberFormatException e){ 
                            System.out.println("Invalid input.");
                        } 
                    }while(check == false);
                }else{
                    System.out.println("There is no product in system.");
                }
                break;
            case "B":
                this.list = this.cartInput.cartList();
                this.uniqueProduct = new HashSet<iproduct>(this.list);
                if(this.list.size() > 0){
                    printCartList();
                }else{
                    System.out.println("There is no product in cart.");
                }
                break;
            case "C":
                this.list = this.cartInput.cartList();
                this.uniqueProduct = new HashSet<iproduct>(this.list);
                ArrayList<iproduct> binlist = this.list;
                if(this.list.size() > 0){
                    boolean check = false;
                    do{
                        printCartList();
                        System.out.println("Please insert product ID.");
                        try{  
                            int terminal_input = Integer.parseInt(App.readInput());
                            for(iproduct var : binlist){
                                if(var.getID() == terminal_input){
                                    boolean checkqty = false;
                                    do{
                                        System.out.println("Please insert Qty.");
                                        try{
                                            int terminal_input_qty = Integer.parseInt(App.readInput());
                                            int count = Collections.frequency(this.list, var);
                                            if(terminal_input_qty > count){
                                                System.out.println("Invalid Qty.");
                                                printCartList();
                                            }else{
                                                for(int index = 0; index < terminal_input_qty; index++){
                                                    this.cartInput.removeFrom(var);
                                                }
                                                checkqty = true;
                                                break;
                                            }
                                        }catch(NumberFormatException e){ 
                                            System.out.println("Invalid Qty.");
                                            printCartList();
                                        }
                                    }while(checkqty == false);
                                }
                            }
                            check = true;
                        }catch(NumberFormatException e){ 
                            System.out.println("Invalid input.");
                        } 
                    }while(check == false);
                }else{
                    System.out.println("There is no product in cart.");
                }
                break;
            case "D":
                this.list = this.cartInput.cartList();
                this.uniqueProduct = new HashSet<iproduct>(this.list);
                if(this.list.size() > 0){
                    boolean checkcart = false;
                    do{
                        System.out.println("Please confirm items in cart.");
                        int totalprice = 0;
                        for(iproduct var : this.uniqueProduct){
                            int count = Collections.frequency(this.list, var);
                            System.out.println("ID: " + var.getID() + " Name: " + var.getName() + " Qty: " + count + " Price Per Unit: " + var.getPrice()
                            + " Baht TotalPrice: " + var.getPrice()*count + " Baht");
                            totalprice+=var.getPrice()*count;
                        }
                        System.out.println("Total Price: " + totalprice + " Baht");
                        System.out.println("Type \"OK\" to checkout.");
                        System.out.println("Type \"Back\" to move up in a menu list.");
                        String terminal_input = App.readInput();
                        switch(terminal_input){
                            case "OK":
                                Boolean checkaddress = false;
                                Boolean confirmaddress = false;
                                ArrayList<String> terminal_address_input = new ArrayList<String>();
                                do{
                                    String temp = "";
                                    do{
                                        System.out.println("Please insert Full Name. E.g. John Doe *Required*");
                                        temp = App.readInput();
                                        String[] namesplit = temp.split(" ");
                                        if(namesplit.length < 2){
                                            temp = "";
                                        }
                                    }while(temp.equals(""));
                                    terminal_address_input.add(temp);
                                    do{
                                        System.out.println("Please insert Address Line 1. *Required* (Street adress, P.O. box, Company name, c/o)");
                                        temp = App.readInput();
                                    }while(temp.equals(""));
                                    terminal_address_input.add(temp);
                                    System.out.println("Please insert Address Line 2. (Apartment, suite, unit, building, floor, etc.)");
                                    terminal_address_input.add(App.readInput());
                                    do{
                                        System.out.println("Please insert City. *Required*");
                                        temp = App.readInput();
                                    }while(temp.equals(""));
                                    terminal_address_input.add(temp);
                                    do{
                                        System.out.println("Please insert State/Province/Region. *Required*");
                                        temp = App.readInput();
                                    }while(temp.equals(""));
                                    terminal_address_input.add(temp);
                                    do{
                                        System.out.println("Please insert ZIP/Postal Code.  *Required*");
                                        temp = App.readInput();
                                    }while(temp.equals(""));
                                    terminal_address_input.add(temp);
                                    do{
                                        System.out.println("Please insert Country.  *Required*");
                                        temp = App.readInput();
                                    }while(temp.equals(""));
                                    terminal_address_input.add(temp);
                                    do{
                                        System.out.println("Please insert confirm Address.");
                                        System.out.println("Full Name:" + terminal_address_input.get(0));
                                        System.out.println("Address Line 1:" + terminal_address_input.get(1));
                                        if(!terminal_address_input.get(2).equals("")){
                                            System.out.println("Address Line 2:" + terminal_address_input.get(2));
                                        }
                                        System.out.println("City:" + terminal_address_input.get(3));
                                        System.out.println("State/Province/Region:" + terminal_address_input.get(4));
                                        System.out.println("ZIP/Postal Code:" + terminal_address_input.get(5));
                                        System.out.println("Country:" + terminal_address_input.get(6));
                                        System.out.println("Type \"OK\" to continue checkout.");
                                        System.out.println("Type \"Edit\" to edit.");
                                        System.out.println("Type \"Back\" to move up in a menu list.");
                                        switch(App.readInput()){
                                            case "OK":
                                                cartInput.Checkout(terminal_address_input);
                                                this.cartInput = new Cart();
                                                confirmaddress = true;
                                                checkcart = true;
                                                break;
                                            case "EDIT":
                                                ArrayList<String> edit_terminal_address_input = new ArrayList<String>();
                                                System.out.println("Please insert Full Name. E.g. John Doe (" + terminal_address_input.get(0) + ")");
                                                System.out.println("Leave it blank if you don't want to change.");
                                                temp = App.readInput();
                                                if(temp.equals("")){
                                                    edit_terminal_address_input.add(terminal_address_input.get(0));
                                                }else{
                                                    do{
                                                        String[] namesplit = temp.split(" ");
                                                        if(namesplit.length < 2){
                                                            temp = "";
                                                            System.out.println("Please insert Full Name. E.g. John Doe (" + terminal_address_input.get(0) + ")");
                                                            System.out.println("Leave it blank if you don't want to change.");
                                                            temp = App.readInput();
                                                        }
                                                        if(temp.equals("")){
                                                            edit_terminal_address_input.add(terminal_address_input.get(0));
                                                            break;
                                                        }
                                                    }while(temp.equals(""));
                                                    edit_terminal_address_input.add(temp);
                                                }
                                                System.out.println("Please insert Address Line 1. (Street address, P.O. box, Company name, c/o) ("
                                                + terminal_address_input.get(1) + ")");
                                                System.out.println("Leave it blank if you don't want to change.");
                                                temp = App.readInput();
                                                if(temp.equals("")){
                                                    edit_terminal_address_input.add(terminal_address_input.get(1));
                                                }else{
                                                    edit_terminal_address_input.add(temp);
                                                }
                                                if(terminal_address_input.get(2).equals("")){
                                                    System.out.println("Please insert Address Line 2. (Apartment, suite, unit, building, floor, etc.)");
                                                    edit_terminal_address_input.add(App.readInput());
                                                }else{
                                                    System.out.println("Please insert Address Line 2. (Apartment, suite, unit, building, floor, etc.) ("
                                                    + terminal_address_input.get(2) + ")");
                                                    System.out.println("Leave it blank if you don't want to change.");
                                                    temp = App.readInput();
                                                    if(temp.equals("")){
                                                        edit_terminal_address_input.add(terminal_address_input.get(2));
                                                    }else{
                                                        edit_terminal_address_input.add(temp);
                                                    }
                                                }
                                                System.out.println("Please insert City. ("
                                                + terminal_address_input.get(3) + ")");
                                                System.out.println("Leave it blank if you don't want to change.");
                                                temp = App.readInput();
                                                if(temp.equals("")){
                                                    edit_terminal_address_input.add(terminal_address_input.get(3));
                                                }else{
                                                    edit_terminal_address_input.add(temp);
                                                }
                                                System.out.println("Please insert State/Province/Region. ("
                                                + terminal_address_input.get(4) + ")");
                                                System.out.println("Leave it blank if you don't want to change.");
                                                temp = App.readInput();
                                                if(temp.equals("")){
                                                    edit_terminal_address_input.add(terminal_address_input.get(4));
                                                }else{
                                                    edit_terminal_address_input.add(temp);
                                                }
                                                System.out.println("Please insert ZIP/Postal Code. ("
                                                + terminal_address_input.get(5) + ")");
                                                System.out.println("Leave it blank if you don't want to change.");
                                                temp = App.readInput();
                                                if(temp.equals("")){
                                                    edit_terminal_address_input.add(terminal_address_input.get(5));
                                                }else{
                                                    edit_terminal_address_input.add(temp);
                                                }
                                                System.out.println("Please insert Country. ("
                                                + terminal_address_input.get(6) + ")");
                                                System.out.println("Leave it blank if you don't want to change.");
                                                temp = App.readInput();
                                                if(temp.equals("")){
                                                    edit_terminal_address_input.add(terminal_address_input.get(6));
                                                }else{
                                                    edit_terminal_address_input.add(temp);
                                                }
                                                terminal_address_input = edit_terminal_address_input;
                                                break;
                                            case "BACK":
                                                confirmaddress = true;
                                            default:
                                                System.out.println("Invalid input.");
                                        }
                                    }while(confirmaddress == false);
                                    checkcart = true;
                                    break;
                                }while(checkaddress == false);
                            case "BACK":
                                checkcart = true;
                                break;
                            default:
                                System.out.println("Invalid input.");
                        }
                    }while(checkcart == false);
                }else{
                    System.out.println("There is no product in cart.");
                }
                break;
            case "BACK":
                App.usermode = Mode.NULL;
                break;
            default:
                System.out.println("Invalid input.");
                break;
        }
    }
    private void printCartList(){
        for(iproduct var : this.uniqueProduct){
            int count = Collections.frequency(this.list, var);
            System.out.println("ID: " + var.getID() + " Name: " + var.getName() + " Qty: " + count + " Price Per Unit: " + var.getPrice()
            + " Baht TotalPrice: " + var.getPrice()*count + " Baht");
        }
    }
}
