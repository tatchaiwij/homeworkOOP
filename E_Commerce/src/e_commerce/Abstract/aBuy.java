package e_commerce.Abstract;

public abstract class aBuy {
    public void printChoice(){
        System.out.println("Type \"A\" for product list avalible to add into cart.");
        System.out.println("Type \"B\" to list product from shopping cart.");
        System.out.println("Type \"C\" to remove product from shopping cart.");
        System.out.println("Type \"D\" to checkout.");
        System.out.println("Type \"Back\" to move up in a menu list.");
    }
    public abstract void buyProduct(String input);
}
