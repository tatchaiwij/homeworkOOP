package e_commerce;

import java.util.Scanner;

public class App {
    public static Mode usermode = Mode.NULL;
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) throws Exception {
        mainmenu();
        scanner.close();
    }
    public static String readInput(){
        String input = scanner.nextLine();
        return input.toUpperCase();
    }
    public static void buyMode(){
        buyMode buy = new buyMode();
        do{
            buy.printChoice();
            String input = readInput();
            buy.buyProduct(input);
        }while(usermode == Mode.USER);
        mainmenu();
    }
    public static void manageMode(){
        manageMode manage = new manageMode();
        do{
            manage.manageProduct();
        }while(usermode == Mode.ADMIN);
        mainmenu();
    }
    public static void mainmenu(){
        System.out.println("What do you want to do ?");
        System.out.println("Type \"A\" if your want to buy.");
        System.out.println("Type \"B\" if your want to manage.");
        System.out.println("Type \"Exit\" if your want to exit.");
        if (usermode == Mode.NULL){
            do{
                String input = readInput();
                switch (input){
                    case "A":
                        usermode = Mode.USER;
                        buyMode();
                        break;
                    case "B":
                        usermode = Mode.ADMIN;
                        manageMode();
                        break;
                    case "EXIT":
                        usermode = Mode.EXIT;
                        break;
                    default:
                        System.out.println("Invalid input.");
                        System.out.println("What do you want to do ?");
                        System.out.println("Type \"A\" if your want to buy.");
                        System.out.println("Type \"Exit\" if your want to exit.");
                        break;
                }
            }while(usermode == Mode.NULL);
        }
    }
}
